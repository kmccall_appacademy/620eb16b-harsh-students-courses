class Student

  attr_accessor :courses

  attr_reader :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    unless @courses.include?(new_course)
      raise "Course conflicts with other courses" if self.courses.any? {|course| course.conflicts_with?(new_course)}
      new_course.students.push(self)
      @courses.push(new_course)
    end

  end

  def course_load
    course_credits = Hash.new(0)
    @courses.each do |course|
      course_credits[course.department]+=course.credits
    end
    course_credits
  end

end
